import { Meteor } from 'meteor/meteor';

if(Meteor.isServer) {
  // When Meteor starts, create new collection in Mongo if not exists.
  Meteor.startup(function () {
      Property = new Meteor.Collection('property');
  });

  // GET /property - returns every message from MongoDB collection.

  Router.route('/properties',{where: 'server'})
    .get(function(){
        var response = Property.find().fetch();
        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
    })

  // POST /message - {message as post data}
  // Add new message in MongoDB collection.

    .post(function(){
        var response;
        if(this.request.body.name === undefined || this.request.body.value === undefined) {
            response = {
                "error" : true,
                "message" : "invalid data"
            };
        } else {

            var data = Property.find({Name : this.request.body.name}).fetch();
            if (data.length == 0) {
                Property.insert({
                    Name : this.request.body.name,
                    Value : this.request.body.value
                });
                response = {
                    "error" : false,
                    "message" : "Property added."
                }
            } else {
                response = {
                    "error" : false,
                    "message" : "Property name is already exist."
                }
            }
        }
        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
    });

Router.route('/properties/:name',{where: 'server'})

    // GET /message/:name - returns specific records

    .get(function(){
        var response;
        if(this.params.name !== undefined) {
            var data = Property.find({Name : this.params.name}).fetch();
            if(data.length > 0) {
                response = data
            } else {
                response = {
                    "error" : true,
                    "message" : "Property not found."
                }
            }
        }
        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
    })

    // PUT /message/:name {message as put data}- update specific records.

    .put(function(){
        var response;
        if(this.params.name !== undefined) {
            var data = Property.find({Name : this.params.name}).fetch();
            if(data.length > 0) {
                if(Property.update({Name : data[0].name},{$set : {Value : this.request.body.value}}) === 1) {
                    response = {
                        "error" : false,
                        "message" : "Property information updated."
                    }
                } else {
                    response = {
                        "error" : true,
                        "message" : "Property information not updated."
                    }
                }
            } else {
                response = {
                    "error" : true,
                    "message" : "User not found."
                }
            }
        }
        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
    })

    // DELETE /message/:name delete specific record.

    .delete(function(){
        var response;
        if(this.params.name !== undefined) {
            var data = Property.find({Name : this.params.name}).fetch();
            if(data.length >  0) {
                if(Property.remove(data[0]._id) === 1) {
                    response = {
                        "error" : false,
                        "message" : "Property deleted."
                    }
                } else {
                    response = {
                        "error" : true,
                        "message" : "Property not deleted."
                    }
                }
            } else {
                response = {
                    "error" : true,
                    "message" : "Property not found."
                }
            }
        }
        this.response.setHeader('Content-Type','application/json');
        this.response.end(JSON.stringify(response));
    });
  }